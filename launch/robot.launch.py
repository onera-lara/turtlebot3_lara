import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, GroupAction, IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, ThisLaunchFileDir, TextSubstitution
from launch_ros.actions import Node, PushRosNamespace, SetRemap


def generate_launch_description():

    use_sim_time = DeclareLaunchArgument('use_sim_time',
                                        default_value=TextSubstitution(text='false'),
                                        description='Use simulation (Gazebo) clock if true')

    usb_port = DeclareLaunchArgument('usb_port',
                                    default_value=TextSubstitution(text='/dev/ttyACM0'),
                                    description='Connected USB port with OpenCR')

    namespace = DeclareLaunchArgument('namespace',
                                    default_value=TextSubstitution(text='/'),
                                    description='Namespace of the Turtle')

    lidar_pkg_dir = LaunchConfiguration(
        'lidar_pkg_dir',
        default=os.path.join(get_package_share_directory('hls_lfcd_lds_driver'), 'launch'))

    tb3_bringup = LaunchConfiguration(
        'turtlebot3_bringup',
        default=os.path.join(get_package_share_directory('turtlebot3_bringup'), 'launch'))

    # include another launch file in the chatter_ns namespace
    turtle = GroupAction(
        actions=[
            # push-ros-namespace to set namespace of included nodes
            PushRosNamespace(LaunchConfiguration('namespace')),
            SetRemap(src='/tf', dst='tf'),
            SetRemap(src='/tf_static', dst='tf_static'),
            IncludeLaunchDescription(
                PythonLaunchDescriptionSource(
                    [tb3_bringup, '/turtlebot3_state_publisher.launch.py']),
                launch_arguments={'use_sim_time': LaunchConfiguration('use_sim_time')}.items(),
            ),
            IncludeLaunchDescription(
                PythonLaunchDescriptionSource([lidar_pkg_dir, '/hlds_laser.launch.py']),
                launch_arguments={'port': '/dev/ttyUSB0', 'frame_id': 'base_scan'}.items(),
            ),
            Node(
                package='turtlebot3_node',
                #name='turblebot3_node', DO NOT SET NAME! The executable actually launches 2 nodes, and it renames both with the same name!
                executable='turtlebot3_ros',
                parameters=[{
                    'opencr.id': 200,
                    'opencr.baud_rate': 1000000,
                    'opencr.protocl_version': 2.0,
                    'wheels.separation': 0.160,
                    'wheels.radius': 0.033,
                    'motors.profile_acceleration_constant': 214.577,
                    'motors.profile_acceleration': 0.0,
                    'sensors.bumper_1': False,
                    'sensors.bumper_2': False,
                    'sensors.illumination': False,
                    'sensors.ir': False,
                    'sensors.sonar': False,
                }],
                arguments=['-i', LaunchConfiguration('usb_port')],
                output='screen'
            ),
        ]
    )

    return LaunchDescription([
        use_sim_time, usb_port, namespace,
        turtle,
    ])

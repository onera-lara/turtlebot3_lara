import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, GroupAction, IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, ThisLaunchFileDir, TextSubstitution
from launch_ros.actions import Node, PushRosNamespace, SetRemap


def generate_launch_description():

    tb3_lara_dir = get_package_share_directory('turtlebot3_lara')
    
    params_file = DeclareLaunchArgument(
        'params_file',
        default_value=os.path.join(tb3_lara_dir, 'param', 'tb3_lara.rviz'),
        description='Full path to the RVIZ parameters file')
    
    return LaunchDescription([
    	params_file,
	DeclareLaunchArgument('namespace',
                            default_value=TextSubstitution(text='/'),
                            description='Namespace of the Turtle'),

	GroupAction(actions=[
            # push-ros-namespace to set namespace of included nodes
            PushRosNamespace(LaunchConfiguration('namespace')),
            SetRemap(src='/tf', dst='tf'),
            SetRemap(src='/tf_static', dst='tf_static'),
            Node(
                name='rviz2', package='rviz2', executable='rviz2',
                arguments=['-d', LaunchConfiguration("params_file")],
                namespace=LaunchConfiguration('namespace')
            ),
        ])
    ])

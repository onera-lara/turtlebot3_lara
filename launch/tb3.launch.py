import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, GroupAction, IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, ThisLaunchFileDir, TextSubstitution
from launch_ros.actions import Node, PushRosNamespace, SetRemap


def generate_launch_description():

    tb3_lara_dir = get_package_share_directory('turtlebot3_lara')

    return LaunchDescription([
	DeclareLaunchArgument('namespace',
                            default_value=TextSubstitution(text='/'),
                            description='Namespace of the Turtle'),

	GroupAction(actions=[
            # push-ros-namespace to set namespace of included nodes
            PushRosNamespace(LaunchConfiguration('namespace')),
            SetRemap(src='/tf', dst='tf'),
            SetRemap(src='/tf_static', dst='tf_static'),
            IncludeLaunchDescription(
                PythonLaunchDescriptionSource(
                    [tb3_lara_dir, '/launch', '/robot.launch.py']),
            ),
            IncludeLaunchDescription(
                PythonLaunchDescriptionSource(
                    [tb3_lara_dir, '/launch', '/navigation.launch.py']),
            ),
        ])
    ])

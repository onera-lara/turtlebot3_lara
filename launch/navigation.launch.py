import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import (DeclareLaunchArgument, GroupAction,
                            IncludeLaunchDescription, SetEnvironmentVariable)
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, PythonExpression, TextSubstitution, PathJoinSubstitution
from launch_ros.actions import PushRosNamespace, SetRemap, Node


def generate_launch_description():
    # Get the launch directory
    bringup_dir = get_package_share_directory('nav2_bringup')
    launch_dir = os.path.join(bringup_dir, 'launch')
    tb3_lara_dir = get_package_share_directory('turtlebot3_lara')#'turtlebot3_navigation2')

    # Create the launch configuration variables
    namespace = DeclareLaunchArgument(
        'namespace',
        default_value=TextSubstitution(text='/'),
        description='Top-level namespace')
    
    map_yaml_file = DeclareLaunchArgument(
        'map',
        default_value=os.path.join(tb3_lara_dir, 'map', 'lara.yaml'),
        description='Full path to map yaml file to load')
    
    use_sim_time = DeclareLaunchArgument(
        'use_sim_time',
        default_value=TextSubstitution(text='False'),
        description='Use simulation (Gazebo) clock if true')

    params_file = DeclareLaunchArgument(
        'params_file',
        default_value=os.path.join(tb3_lara_dir, 'param', 'burger.yaml'),
        description='Full path to the ROS2 parameters file to use for all launched nodes')

    bt = LaunchConfiguration('bt_xml_filename',
        default=os.path.join(
            get_package_share_directory('nav2_bt_navigator'),
            'behavior_trees', 'navigate_w_replanning_and_recovery.xml'))

    # Navigation Stack
    nav2 = GroupAction([
        PushRosNamespace(namespace=LaunchConfiguration('namespace')),

        SetRemap(src='/tf', dst='tf'),
        SetRemap(src='/tf_static', dst='tf_static'),
        SetRemap(src='/scan', dst=PathJoinSubstitution([LaunchConfiguration('namespace'), TextSubstitution(text='scan')])),

        IncludeLaunchDescription(
                PythonLaunchDescriptionSource(os.path.join(launch_dir, 'navigation_launch.py')),
                launch_arguments={'namespace': LaunchConfiguration('namespace'),
                                  'use_sim_time': LaunchConfiguration('use_sim_time'),
                                  'autostart': 'true',
                                  'params_file': LaunchConfiguration('params_file'),
                                  'default_bt_xml_filename': bt,
                                  'use_lifecycle_mgr': 'false',
                                  'map_subscribe_transient_local': 'true'}.items()),

        Node(package='tf2_ros',
            executable='static_transform_publisher',
            name='static_tf_map_odom',
            arguments=['0', '0', '0', '0', '0', '0', 'map', 'odom']),

        Node(package='nav2_map_server',
            executable='map_server',
            name='map_server',
            output='screen',
            parameters=[{'use_sim_time': False,
                        'yaml_filename': LaunchConfiguration('map')}]),

        Node(package='nav2_lifecycle_manager',
            executable='lifecycle_manager',
            name='lifecycle_manager_localization',
            output='screen',
            parameters=[{'use_sim_time': LaunchConfiguration('use_sim_time'),
                        'autostart': True,
                        'node_names': ['map_server']}])
    ])

    # Create the launch description and populate
    ld = LaunchDescription()

    # Declare the launch options
    ld.add_action(namespace)
    ld.add_action(map_yaml_file)
    ld.add_action(use_sim_time)
    ld.add_action(params_file)

    # Add the actions to launch all of the navigation nodes
    ld.add_action(nav2)

    return ld
